#![no_main]

use libfuzzer_sys::fuzz_target;

extern crate base64;

fuzz_target!(|data: &[u8]| {
    let _ = base64::base64_encode(data);
});
