use std::io::{self, Read};

use base64::base64_encode;

fn main() {
    let mut buf: Vec<u8> = Vec::new();
    let mut stdin = io::stdin();
    stdin
        .read_to_end(&mut buf)
        .expect("Failed to read from stdin");

    println!("{}", base64_encode(&buf).iter().collect::<String>());
}
