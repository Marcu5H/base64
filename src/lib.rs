#![feature(test)]
extern crate test;

pub fn base64_encode(data: &[u8]) -> Vec<char> {
    macro_rules! next_byte {
        ($q:expr, $data_iter:expr) => {
            match $data_iter.next() {
                Some(b) => b,
                None => {
                    $q += 1;
                    &0
                }
            }
        };
    }

    let mut encoded: Vec<char> = Vec::new();
    let b64_map: [char; 64] = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1',
        '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
    ];

    let mut data_iter = data.iter();
    loop {
        let mut q: u8 = 0;
        let a = next_byte!(q, data_iter);
        let b = next_byte!(q, data_iter);
        let c = next_byte!(q, data_iter);

        if q >= 3 {
            break;
        }

        encoded.push(b64_map[(a >> 2) as usize]);
        encoded.push(b64_map[(((a & 0b00000011) << 4) + ((b & 0b11110000) >> 4)) as usize]);
        if q > 0 {
            match q {
                1 => encoded.push(b64_map[(((b & 0b00001111) << 2) + (c >> 6)) as usize]),
                _ => encoded.push('='),
            }
            encoded.push('=');
            break;
        }
        encoded.push(b64_map[(((b & 0b00001111) << 2) + (c >> 6)) as usize]);
        encoded.push(b64_map[(c & 0b00111111) as usize]);
    }

    return encoded;
}

pub fn base64_decode(encoded_data: &[char]) -> Vec<u8> {
    macro_rules! next_char {
        ($q:expr, $data_iter:expr) => {
            match $data_iter.next() {
                Some(b) => {
                    if *b == '=' {
                        $q += 1;
                    }
                    *b
                }
                None => {
                    $q += 1;
                    'A'
                }
            }
        };
    }
    macro_rules! char_to_sextet_index {
        ($c:expr) => {
            match $c {
                'A' => 0,
                'B' => 1,
                'C' => 2,
                'D' => 3,
                'E' => 4,
                'F' => 5,
                'G' => 6,
                'H' => 7,
                'I' => 8,
                'J' => 9,
                'K' => 10,
                'L' => 11,
                'M' => 12,
                'N' => 13,
                'O' => 14,
                'P' => 15,
                'Q' => 16,
                'R' => 17,
                'S' => 18,
                'T' => 19,
                'U' => 20,
                'V' => 21,
                'W' => 22,
                'X' => 23,
                'Y' => 24,
                'Z' => 25,
                'a' => 26,
                'b' => 27,
                'c' => 28,
                'd' => 29,
                'e' => 30,
                'f' => 31,
                'g' => 32,
                'h' => 33,
                'i' => 34,
                'j' => 35,
                'k' => 36,
                'l' => 37,
                'm' => 38,
                'n' => 39,
                'o' => 40,
                'p' => 41,
                'q' => 42,
                'r' => 43,
                's' => 44,
                't' => 45,
                'u' => 46,
                'v' => 47,
                'w' => 48,
                'x' => 49,
                'y' => 50,
                'z' => 51,
                '0' => 52,
                '1' => 53,
                '2' => 54,
                '3' => 55,
                '4' => 56,
                '5' => 57,
                '6' => 58,
                '7' => 59,
                '8' => 60,
                '9' => 61,
                '+' => 62,
                '/' => 63,
                '=' => 0,
                _ => panic!("{} Invalid character", $c),
            }
        };
    }

    let mut decoded: Vec<u8> = Vec::new();
    let sextet_map: [u8; 64] = [
        0b000000, 0b000001, 0b000010, 0b000011, 0b000100, 0b000101, 0b000110, 0b000111, 0b001000,
        0b001001, 0b001010, 0b001011, 0b001100, 0b001101, 0b001110, 0b001111, 0b010000, 0b010001,
        0b010010, 0b010011, 0b010100, 0b010101, 0b010110, 0b010111, 0b011000, 0b011001, 0b011010,
        0b011011, 0b011100, 0b011101, 0b011110, 0b011111, 0b100000, 0b100001, 0b100010, 0b100011,
        0b100100, 0b100101, 0b100110, 0b100111, 0b101000, 0b101001, 0b101010, 0b101011, 0b101100,
        0b101101, 0b101110, 0b101111, 0b110000, 0b110001, 0b110010, 0b110011, 0b110100, 0b110101,
        0b110110, 0b110111, 0b111000, 0b111001, 0b111010, 0b111011, 0b111100, 0b111101, 0b111110,
        0b111111,
    ];

    let mut data_iter = encoded_data.iter();
    loop {
        let mut q: u8 = 0;
        let a = sextet_map[char_to_sextet_index!(next_char!(q, data_iter)) as usize];
        let b = sextet_map[char_to_sextet_index!(next_char!(q, data_iter)) as usize];
        let c = sextet_map[char_to_sextet_index!(next_char!(q, data_iter)) as usize];
        let d = sextet_map[char_to_sextet_index!(next_char!(q, data_iter)) as usize];

        if q == 4 {
            break;
        }

        match q {
            0 => {
                decoded.push((a << 2) + (b >> 4));
                decoded.push(((b & 0b001111) << 4) + (c >> 2));
                decoded.push((c << 6) + d);
            }
            1 => {
                decoded.push((a << 2) + (b >> 4));
                decoded.push(((b & 0b001111) << 4) + (c >> 2));
                decoded.push(c << 6);
                break;
            }
            2 => {
                decoded.push((a << 2) + (b >> 4));
                break;
            }
            3 => {
                decoded.push(a << 2);
                break;
            }
            _ => unreachable!(),
        }
    }

    return decoded;
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    fn str_to_vec(s: &str) -> Vec<char> {
        let mut __vec: Vec<char> = Vec::new();
        s.chars().for_each(|c| __vec.push(c));
        return __vec;
    }

    #[test]
    fn encode1() {
        assert_eq!(base64_encode("test".as_bytes()), str_to_vec("dGVzdA=="));
    }

    #[test]
    fn encode2() {
        assert_eq!(base64_encode("Man".as_bytes()), str_to_vec("TWFu"));
    }

    #[test]
    fn decode1() {
        assert_eq!(base64_decode(&str_to_vec("dGVzdA==")), "test".as_bytes());
    }

    #[test]
    fn decode2() {
        assert_eq!(base64_decode(&str_to_vec("TWFu")), "Man".as_bytes());
    }

    #[test]
    fn decode3() {
        let input = "This is a test asdasdj aklsjd lkajsdlkjaslkdj alksjd lkaj".as_bytes();
        assert_eq!(base64_decode(&base64_encode(input)), input);
    }

    #[test]
    fn decode4() {
        let input = "askjdj lakjsdl kjasd8723077324+95872+98yu28ty2qåheu8iajf8hyu".as_bytes();
        assert_eq!(base64_decode(&base64_encode(input)), input);
    }

    #[test]
    fn decode5() {
        let input =
            "akjsldk ajsd0987397623746238764827364+8174+81964+21985634972563087465".as_bytes();
        assert_eq!(base64_decode(&base64_encode(input)), input);
    }

    #[test]
    fn decode6() {
        let input = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA".as_bytes();
        assert_eq!(base64_decode(&base64_encode(input)), input);
    }

    #[bench]
    fn encode_100_bytes(b: &mut Bencher) {
        b.iter(||
            base64_encode("2+4935askjdlaksjdlkajsdlkjasdlkjjwout3w04ncv0873245gsdftuy348tyu0ishjdf08y23rsiajdf08y0823asjd8fdmnl".as_bytes())
        );
    }

    #[bench]
    fn decode_136_bytes(b: &mut Bencher) {
        b.iter(|| {
            base64_decode(&[
                'M', 'i', 's', '0', 'O', 'T', 'M', '1', 'Y', 'X', 'N', 'r', 'a', 'm', 'R', 's',
                'Y', 'W', 't', 'z', 'a', 'm', 'R', 's', 'a', '2', 'F', 'q', 'c', '2', 'R', 's',
                'a', '2', 'p', 'h', 'c', '2', 'R', 's', 'a', '2', 'p', 'q', 'd', '2', '9', '1',
                'd', 'D', 'N', '3', 'M', 'D', 'R', 'u', 'Y', '3', 'Y', 'w', 'O', 'D', 'c', 'z',
                'M', 'j', 'Q', '1', 'Z', '3', 'N', 'k', 'Z', 'n', 'R', '1', 'e', 'T', 'M', '0',
                'O', 'H', 'R', '5', 'd', 'T', 'B', 'p', 'c', '2', 'h', 'q', 'Z', 'G', 'Y', 'w',
                'O', 'H', 'k', 'y', 'M', '3', 'J', 'z', 'a', 'W', 'F', 'q', 'Z', 'G', 'Y', 'w',
                'O', 'H', 'k', 'w', 'O', 'D', 'I', 'z', 'Y', 'X', 'N', 'q', 'Z', 'D', 'h', 'm',
                'Z', 'G', '1', 'u', 'b', 'A', '=', '=',
            ])
        });
    }

    #[bench]
    fn decode_1368_bytes(b: &mut Bencher) {
        b.iter(|| {
            base64_decode(&[
                '1', 'O', '2', 'd', 'o', 't', 'Q', 'W', 'V', 'S', 'J', 'S', 'x', 'C', '7', 'D',
                'H', 'c', 'I', '3', '9', 'c', 'd', 'O', 'i', '1', 'z', 'B', '4', '8', 'V', 'T',
                'l', 'W', 'O', 'J', 'A', 'x', 'z', 's', 'b', 'Y', 't', 'D', 'F', 'H', 'S', 'E',
                'B', 'T', 'u', 'J', 'T', 'i', 'f', 'Y', 'p', '3', 'R', 'u', 'k', '+', 'H', '8',
                'g', 's', 'a', 'M', 'p', '2', '9', '7', 'F', 's', 'U', 'R', 'K', '4', 'p', 'Q',
                'g', 'b', 'O', 'Y', 'x', 'z', 'c', 't', 'Q', 'N', 'r', 'A', '6', 'A', 'S', 'X',
                'j', 'G', 'i', 'Q', 'c', 'U', 'y', '5', 'm', '7', 'h', '0', 'O', 'L', 't', 'b',
                'a', 'S', 'X', 'g', 'y', 'b', 'L', 'O', 'y', 'z', 'n', 'L', 'P', '6', '0', '0',
                'y', 'l', 'N', 'v', 'U', 'X', 'm', 'X', 'l', 'C', 'j', 'm', 'D', 'H', 'R', 'k',
                'l', 'z', '7', 'R', 'w', 'V', 'r', 'b', 'x', 'q', 'l', 'v', 'E', '5', 'g', 'w',
                '6', 'P', 'w', 'O', '+', 'y', 'R', 'm', 's', 'Z', 'g', 'g', 'm', 'f', 'H', 'w',
                'U', 'N', 'q', 'R', 'x', 'F', 'V', 'J', 'd', 'D', 'g', 'U', 'U', 'C', 'A', 'J',
                'Q', 'Q', 'B', '0', '5', 'k', 'E', 'K', 'E', '1', 'o', 'E', 'B', 'g', 'v', 'X',
                'r', 'Z', 'e', 'J', 'a', 'H', '/', 'X', 'h', 'p', 'H', '3', 'p', 'h', 'L', 'S',
                'm', 'Z', 'Y', 'X', 'e', 'L', 'R', 'N', 'L', '6', 'a', '8', '3', 'G', 'F', 'X',
                '/', 'T', 'S', 'W', '5', 'S', 'e', 'R', '9', 't', '8', 'H', 'm', 's', 'Q', 'I',
                'D', 't', 'u', 'X', 'Q', 'z', 'K', 'l', '/', 'k', 'K', 'M', 'D', 'E', 'v', 'E',
                '0', 'O', 't', 'F', 't', 'M', 't', 't', 'w', 'y', 'A', 'W', 'V', 'J', 'v', 'r',
                'Y', 'Q', '2', 'e', 'C', 'V', 'W', 'j', 'e', 'l', 's', 'H', '2', '4', 'c', 'j',
                'e', 'm', 'P', 's', 'L', 'u', 'D', 'A', 'w', 'D', 'a', 'X', '+', 'k', 'x', '3',
                '/', 'w', 'x', 'G', 'j', 'T', 'A', 'G', '1', 'm', 'W', 'j', 'R', 'e', 'D', 'R',
                'U', 'F', '5', 'o', '2', 'S', 'N', 'a', 'Y', '5', 'F', 'X', 'D', 'e', '7', 'h',
                'k', 'h', '3', '9', 'C', 'k', 'U', 'E', 'N', 'Z', 'k', 'A', '8', '6', 'L', 'c',
                'V', 'T', 'n', '2', 'j', 'Y', '5', 'p', 'g', 's', '5', 'K', 'b', 'Z', 'n', 'Y',
                'l', 'I', 'V', 'm', 'v', 't', 'h', 'h', 'Q', 'K', 'G', 'z', '9', 'Y', 'x', 'r',
                'X', 'n', '7', 'R', 'Y', 'W', '2', 'j', 'J', 'K', 'w', 'J', '2', '5', '0', 'W',
                'A', 's', 'j', 'T', '8', 'w', 'V', '5', 'O', 'k', 'N', 'd', 'y', '2', '2', 'V',
                'y', 't', 'P', 'q', 'k', 'H', '0', 'e', 'E', '3', 'c', 'L', 'K', '2', 'E', '5',
                'Z', 'M', 'q', '1', '0', '7', 'D', 'U', '6', 'u', 'N', 'i', 'h', 't', 'W', 'q',
                'B', 's', 'c', 'H', '4', 'q', 'j', 'b', 'b', 'Q', 'Y', 'R', '7', 'y', '8', 'w',
                'I', 'S', 'V', 'K', 'P', 'F', 'M', 'L', 'V', 'D', 'H', 'R', 'Q', 'c', 'L', 'r',
                'V', 's', 'D', 'A', 'Z', '8', 'e', 'l', 'Z', 'K', 'F', 't', 'X', 'p', 'h', 'J',
                'D', 'p', '4', 'v', 'p', 'a', 'k', 'O', 'g', 'H', 'Q', '9', 'N', 'E', 'd', 'L',
                'v', 'e', 'F', 'U', 'i', 'Q', 'J', '0', 'B', 'x', 'u', 'G', 'J', 'H', 'J', 'j',
                '7', 'r', 'V', 'O', 'G', 'A', 'N', 'G', 'P', 'U', 'i', 'I', 'C', 'g', 'K', 'y',
                '8', 'E', 'X', '/', '/', 'U', '0', 'I', 'n', 'i', 'k', '4', '+', 't', 'C', 'u',
                'c', 'S', '3', '6', 'd', 'F', 'z', '6', 'n', 'k', 'C', 'f', 'f', '4', 'w', 'S',
                'O', 'K', 'f', 'M', '2', 'e', 'Q', 'w', 'l', 'i', 'G', '+', 'Y', '9', 'R', 'I',
                'h', 'D', 'X', 'i', 'l', 'B', 'c', 'd', 'r', 'Z', 'X', 'n', 'C', 'A', 'E', 'i',
                'b', '1', 'b', 'V', 'h', '/', 'f', 'c', '1', 'l', 'q', 'F', '6', 'P', 'u', 'n',
                'N', 'i', 'o', 'U', '+', 'J', 'z', 'k', 'Y', 'R', 'R', 'i', 'm', 'E', 'U', 'w',
                'X', 's', 'G', 'V', 'Z', 'E', 'X', 'l', 'S', '/', 'm', 'C', 'H', '5', 'w', 'b',
                'E', 'x', 'k', 'H', '3', 'G', 'R', 'D', '/', '8', 'u', '2', 'm', '8', 'n', 'y',
                'X', 'U', '4', 'H', 'E', 'S', '5', '5', 'K', '7', 'l', 'V', 'G', 'u', 'F', '2',
                'y', 'M', 'V', 'B', 'E', 'w', 'v', 'A', 'N', 'Q', '9', 'J', 'm', 'B', 'p', 'm',
                '7', '1', 'n', 'G', 'Y', 'L', '8', 'J', 't', 'H', '+', '3', '4', '4', '1', '5',
                '9', 'n', 'm', 'J', '6', 'v', 'N', 'P', 'I', 'R', 'P', 'N', 'k', 'k', 'S', '9',
                'b', 'P', 'x', 'I', '4', 'o', 'p', 'j', '+', 'v', 'm', 'X', 'P', 'D', 'P', 'L',
                '6', 'k', 'o', 'o', 'z', 'S', 'z', 'q', '6', 'M', 't', '5', 'A', 'f', 'J', 'W',
                '3', 'j', 'n', '0', 'T', '/', 'h', 'p', 'Z', 'S', 'j', '+', 'Q', 'p', '6', 'B',
                'B', '9', '4', 'N', 'g', 's', 'Q', 't', 'm', 'T', 'm', 'o', 'm', '7', 'B', '+',
                'L', 'o', '9', '/', 'E', '+', 'O', 'u', '7', 'U', 'T', 'B', 'O', 'G', 'A', 't',
                'g', 'B', 'z', 'U', 'J', 'k', 't', 'J', '7', 'H', 'M', 'P', 'f', 'U', 'U', 'i',
                'q', 'G', '+', 'v', 'N', 'j', 'A', '7', 'x', '1', 'X', 'E', 'd', 'U', 'O', '5',
                '5', 'H', 'B', 'l', 'V', '4', 'y', '/', 'w', 'E', 'W', 'Y', 'r', 'b', 'y', '9',
                'x', 'd', 'U', 'f', '0', '/', 'j', 'u', 'w', 'G', 'v', 'g', '0', '0', 'e', 'P',
                'U', 'l', 'K', '9', 'v', 'a', 'e', '2', 'D', '6', '/', 'M', 'L', '0', 'i', 'o',
                '3', 'f', 'k', 'v', 'x', 's', 'F', '7', 'B', 'q', 'T', 'u', 'h', 'h', 'u', '8',
                '/', 'K', 'v', 'N', 'K', '/', 'h', '2', 'g', 'L', 'x', 'I', 'W', '7', 'F', 'd',
                'L', 'h', 'V', 'N', 'p', 'G', 'k', 'Z', 's', 'b', '6', 'q', 'L', 'A', 'U', 'I',
                'j', 'K', 'k', '/', 'F', 'u', 'Y', 'a', '/', 'i', '9', 'a', 'H', 'Q', 'C', 'c',
                't', 'P', 'O', '0', 'L', 'x', 'c', 'P', '+', '0', 'c', 'q', 'S', 'O', 'P', 'A',
                'w', '8', 'D', 'k', 'p', '3', 'J', '5', 'e', 'P', 'B', '4', 'm', 'H', 't', 'R',
                'o', 'd', 'S', 'W', 'y', 'L', '5', '0', 'H', 'P', '+', 'M', 'W', 'C', 'Q', 'H',
                'w', 'R', 'j', 'G', 'j', 'x', 'Y', 'e', 'X', 'T', 'S', '/', '4', 'P', '4', 'G',
                'L', 'k', '8', 'k', '6', 'X', 'b', 'i', 'W', 'P', 'P', 'D', 'G', 'D', 'F', 'p',
                '+', 'q', '+', 'c', 'o', 'F', 'C', '4', 'M', 't', 'T', 't', 'I', 'T', 'l', 'V',
                'N', '9', 'g', '1', 'l', 'N', 'W', 'x', 'F', 'f', 'u', 'Z', '4', 'q', 'j', 'c',
                '3', 'B', 'k', 'x', 'n', 'v', 'b', '6', 'c', 'w', 'X', '4', 'm', '4', 'j', 'T',
                '7', 'd', 'M', 'l', 'C', 't', 'z', 'E', 'u', 'K', 'Y', 'G', 'a', 'c', 'C', 'I',
                'b', '/', 'f', 'x', 'g', 'p', 'Y', 'v', '5', 'F', 'X', 'a', 'k', 'A', 'r', 'P',
                'w', 'Z', 's', 'j', '5', 'H', 'a', 'b', 'C', 'x', 'D', 'z', 'o', 'x', 'o', '0',
                'k', 'H', 'v', '8', '+', 'V', 'D', 'z', 'i', 'n', 'M', 'R', 'Q', 'q', 'H', '+',
                'h', 'k', 'z', '1', 'K', 'R', 'p', 'p', 'Y', 'e', '3', '4', 'g', 'a', 'E', 'I',
                'M', 'a', 'r', 'K', 'v', 'w', 't', 'j', 'C', 'z', '+', 'l', '1', 'C', 'V', 'F',
                'D', 'H', '1', 'E', 'e', 'r', 'U', 'J', 'l', '8', 'x', '/', 'p', 'k', '4', 'R',
                'b', 'A', 'g', '6', 'X', 'z', 'A', '1', 'I', 'k', 'X', 'j', '5', 'Q', 'h', 'd',
                'l', '9', '+', 'B', 'H', 'f', 'T', 'B', 'Q', 'Z', 'v', 'r', 'p', 'I', '8', 'J',
                'r', 'm', 'W', 'X', 'K', 'n', 'T', 'y', 'h', 'u', '9', 'M', 'g', 'h', 'g', '8',
                'F', 'f', 'u', '5', 'Y', 'Y', '/', '8', 'M', 'O', 'U', 'S', 'W', 'w', 'm', 'h',
                'X', 'w', 'K', 'j', 't', 'A', 'f', 'j', 'C', 'E', '/', 'K', 'C', 'V', 'V', 'f',
                'B', 'L', 'f', 'n', '3', 'C', 'N', 'c', 'n', '/', 'x', 'a', 'g', 'g', 'P', 'G',
                'R', 'B', 'E', 'Z', 't', 'w', 'q', 'x', 'H', 'P', 'V', 'E', 'n', 'b', 'B', 'C',
                '0', 'M', '8', '5', 'l', 'p', 'J', 'A', 'e', 'c', 'A', 'v', '4', 'm', 'l', '/',
                '7', 'm', 'v', 'i', 'Q', '8', 'p', '7', 'L', 'V', 'K', 'L', 'Y', 'c', 'K', 'F',
                'c', 'v', 'S', '5', '1',
            ])
        });
    }
}
